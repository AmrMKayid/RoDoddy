import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

import Button from './common/Button';

class Home extends Component {

    render() {
        return (
            <View>
                <Button
                    title='Login'
                    onPress={() => this.props.navigation.navigate('Login')}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#f16138',
        height: 70,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 10
    }

});

export default Home;