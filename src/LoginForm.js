import React, {Component} from 'react';
import {Text, View} from 'react-native';

import {Button, Card, CardItem, Input} from './common';


class LoginForm extends Component {

    constructor() {
        super();
        this.state = {
            username: '',
            password: ''
        };
    }

    _onLoginPressed() {
        console.log("Login");
    }

    render() {
        return (
            <Card>
                <CardItem>
                    <Input
                        label='Email'
                        placeholder='Enter your email'
                        secureTextEntry={false}
                        onChangeText={(username) => this.setState({username})}
                    />
                </CardItem>

                <CardItem>
                    <Input
                        label='Password'
                        placeholder='Enter your Password'
                        secureTextEntry
                        onChangeText={(password) => this.setState({password})}
                    />
                </CardItem>

                <CardItem>
                    <Button onPress={this._onLoginPressed.bind(this)} >Login</Button>
                </CardItem>
            </Card>
        );
    }
}

export default LoginForm;